import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import LoginPage from './pages/Login';
import InicialPage from './pages/Inicial';
import RegistrarPage from './pages/Registrar';
import AuthLoadingScreen from './pages/Auth';

const PrincipalPage = createStackNavigator({
    InicialPage: {
        screen: InicialPage,
        navigationOptions: (props) => ({
            headerStyle: {
                backgroundColor: '#f2f6f9'
            },
            headerTitle: <Text><Icon name="rocket" size={30} color="#900" /> Titulo</Text>
        })
    }
})

const AppInicial = createSwitchNavigator({
    Auth: {
        screen: AuthLoadingScreen
    },
    Login: {
        screen: LoginPage
    },
    Registrar: {
        screen: RegistrarPage
    },
    Principal: {
        screen: PrincipalPage
    }
}, { initialRouteName: "Auth" })

export default createAppContainer(AppInicial)