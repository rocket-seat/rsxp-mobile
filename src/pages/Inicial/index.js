import React from 'react';
import { ScrollView, StyleSheet, View, Text, ToastAndroid, Switch, FlatList, AsyncStorage, TouchableOpacity } from 'react-native';
import BluetoothSerial from 'react-native-bluetooth-serial-next'
import Reactotron from 'reactotron-react-native';

class Inicial extends React.Component {

    state = {
        isEnabled: false,
        discovering: true,
        devices: [],
        unpairedDevices: []
    }

    componentDidMount() {
        this.setState({ discovering: true })

        BluetoothSerial.isEnabled().then((res) => { this.setState({ isEnabled: res }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
        BluetoothSerial.list().then((res) => { this.setState({ devices: res, discovering: false }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
        BluetoothSerial.listUnpaired().then((res) => { this.setState({ unpairedDevices: res }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))

        BluetoothSerial.on('bluetoothEnabled', () => {
            this.setState({ discovering: true })

            BluetoothSerial.isEnabled().then((res) => { this.setState({ isEnabled: res }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
            BluetoothSerial.list().then((res) => { this.setState({ devices: res, discovering: false }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
            BluetoothSerial.listUnpaired().then((res) => { this.setState({ unpairedDevices: res }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))

            BluetoothSerial.on('bluetoothDisabled', () => {
                this.setState({ isEnabled: false, devices: [] })
            })

            BluetoothSerial.on('error', (err) => Reactotron.log('error', err.message))
        })

    }

    enable() {
        this.setState({ discovering: true })
        BluetoothSerial.enable().then((res) => this.setState({ isEnabled: res, discovering: false })).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
    }

    disable() {
        Promise.all([
            BluetoothSerial.disconnectAll(),
            BluetoothSerial.disable(),
        ]).then((res) => {
            this.setState({ isEnabled: false, devices: [], unpairedDevices: [] })
        }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
    }

    toggleBluetooth(value) {
        if (value === true) {
            this.enable()
        } else {
            this.disable()
        }
    }

    sair = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Login')
    }

    refresh = async () => {
        this.setState({ discovering: true, devices: [], unpairedDevices: [] })

        BluetoothSerial.list().then((res) => { this.setState({ devices: res, discovering: false }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
        BluetoothSerial.listUnpaired().then((res) => { this.setState({ unpairedDevices: res }) }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
    }

    unpair = (item) => {
        BluetoothSerial.unpairDevice(item.id).then((res) => { this.refresh() }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
        BluetoothSerial.disconnect(item.id).then((res) => { this.refresh() }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))

        ToastAndroid.show(item.name + ' disconnected', ToastAndroid.LONG)
    }

    pair = (item) => {
        BluetoothSerial.pairDevice(item.id).then((res) => { this.refresh() }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))
        BluetoothSerial.connect(item.id).then((res) => { this.refresh() }).catch((err) => ToastAndroid.show(err.message, ToastAndroid.LONG))

        ToastAndroid.show(item.name + ' connected', ToastAndroid.LONG)
    }

    _renderItemUnparied(item) {
        return (
            <View style={styles.deviceNameWrap}>
                <TouchableOpacity onPress={() => this.pair(item)}>
                    <Text style={styles.deviceName}>{item.id} - {item.name}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    _renderItem(item) {
        return (
            <View style={styles.deviceNameWrap}>
                <TouchableOpacity onPress={() => this.unpair(item)}>
                    <Text style={styles.deviceName}>{item.id} - {item.name}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <ScrollView>
                <Text>tela de inicio</Text>

                <Text>{JSON.stringify(this.state)}</Text>

                <TouchableOpacity onPress={this.sair}>
                    <Text>Sair</Text>
                </TouchableOpacity>

                <Switch value={this.state.isEnabled} onValueChange={(val) => this.toggleBluetooth(val)} />

                {this.state.isEnabled && this.state.discovering &&
                    <View>
                        <Text>Aguarde...</Text>
                    </View>
                }

                {this.state.isEnabled && !this.state.discovering &&
                    <View>
                        <FlatList style={{ flex: 1 }} data={this.state.devices} keyExtractor={item => item.id} renderItem={({ item }) => this._renderItem(item)} />

                        <FlatList style={{ flex: 1 }} data={this.state.unpairedDevices} keyExtractor={item => item.id} renderItem={({ item }) => this._renderItemUnparied(item)} />

                        <TouchableOpacity onPress={this.refresh}>
                            <Text>Refresh</Text>
                        </TouchableOpacity>
                    </View>
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    toolbar: {
        paddingTop: 30,
        paddingBottom: 30,
        flexDirection: 'row'
    },
    toolbarButton: {
        width: 50,
        marginTop: 8,
    },
    toolbarTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        marginTop: 6
    },
    deviceName: {
        fontSize: 17,
        color: "black"
    },
    deviceNameWrap: {
        margin: 10,
        borderBottomWidth: 1
    }
});

export default Inicial