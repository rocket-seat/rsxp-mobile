import React, { Component } from 'react'
import { AsyncStorage, StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

import FingerprintScanner from 'react-native-fingerprint-scanner';

class FingerprintPopup extends Component {

    state = { errorMessage: undefined }

    componentDidMount() {
        FingerprintScanner
            .authenticate({ onAttempt: this.handleAuthenticationAttempted })
            .then(() => this.props.ir())
            .catch(this.handleAuthenticationAttempted);
    }

    componentWillUnmount = () => FingerprintScanner.release()

    handleAuthenticationAttempted = (error) => this.setState({ errorMessage: error.message });

    render() {
        const { errorMessage } = this.state;
        const { style } = this.props;

        return (
            <View style={styles.container}>
                <View style={[styles.contentContainer, style]}>

                    <Image style={styles.logo} source={require('../../assets/finger_print.png')} />
                    <Text style={styles.heading}>Autenticação{'\n'}com Digital</Text>

                    <Text style={[styles.description, { color: (this.state.errorMessage) ? '#ea3d13' : '#a5a5a5' }]} >
                        {errorMessage || 'Escaneie sua digital\npara autenticar e continuar'}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 150,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#ff3366",
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    logo: {
        marginVertical: 45,
    },
    heading: {
        textAlign: 'center',
        color: '#00a4de',
        fontSize: 21,
    },
    description: {
        textAlign: 'center',
        height: 65,
        fontSize: 18,
        marginVertical: 10,
        marginHorizontal: 20,
    },
    buttonContainer: {
        padding: 20,
    },
    buttonText: {
        color: '#8fbc5a',
        fontSize: 15,
        fontWeight: 'bold',
    }
})

export default FingerprintPopup