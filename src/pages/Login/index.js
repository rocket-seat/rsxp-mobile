import React from 'react';
import { View, Text, AsyncStorage, TouchableOpacity } from 'react-native';

import FingerprintPopup from '../Auth/FingerprintPopup'

const Login = ({ navigation }) => {
    registrar = () => {
        navigation.navigate('Registrar')
    }

    ir = async () => {
        await AsyncStorage.setItem('userToken', 'abc');
        navigation.navigate('Principal')
    }

    return (
        <View>
            <Text>tela de login</Text>

            <TouchableOpacity onPress={registrar}>
                <Text>Registrar</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={ir}>
                <Text>Entra</Text>
            </TouchableOpacity>

            <FingerprintPopup ir={ir} />
        </View>
    )
}

export default Login